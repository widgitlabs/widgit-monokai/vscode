/*global module, require*/
module.exports = function (grunt) {
  // Load multiple grunt tasks using globbing patterns
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Run JSHint
    jshint: {
      all: ['**/*.js', '**/*.json'],
      jshintrc: '.jshintrc',
    },

    // Run StyleLint
    stylelint: {
      files: ['**/*.css', '**/*.scss'],
    },

    // Run MarkdownLint
    markdownlint: {
      full: {
        options: {
          config: {
            default: true,
          },
        },
        src: [
          '**/*.md',
          '!out/**/*.md',
          '!node_modules/**/*.md',
          '!vendor/**/*.md',
        ],
      },
    },

    // Build
    exec: {
      build: {
        cmd: 'mkdir -p out && vsce package -o out/',
      },
    },
  });

  // Build task(s).
  grunt.registerTask('lint_jshint', ['jshint']);
  grunt.registerTask('lint_stylelint', ['stylelint']);
  grunt.registerTask('lint_markdownlint', ['markdownlint']);

  grunt.registerTask('stage', ['exec:build']);

  grunt.registerTask('test', [
    'lint_jshint',
    'lint_stylelint',
    'lint_markdownlint',
  ]);

  grunt.registerTask('build', ['test', 'stage']);
};
