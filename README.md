# Widgit Monokai Theme

The official Widgit Labs theme, based on [One Monokai](https://marketplace.visualstudio.com/items?itemName=azemoh.one-monokai)

[![License](https://img.shields.io/badge/license-MIT-green.svg)](https://gitlab.com/widgitlabs/widgit-monokai/vscode/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/widgit-monokai/vscode/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/widgit-monokai/vscode/pipelines)
[![Version](https://img.shields.io/visual-studio-marketplace/v/widgitlabs.widgit-monokai?color=blue)](https://marketplace.visualstudio.com/items?itemName=widgitlabs.widgit-monokai)
[![Installs](https://img.shields.io/visual-studio-marketplace/i/widgitlabs.widgit-monokai)](https://marketplace.visualstudio.com/items?itemName=widgitlabs.widgit-monokai)
[![Ratings](https://img.shields.io/visual-studio-marketplace/r/widgitlabs.widgit-monokai)](https://marketplace.visualstudio.com/items?itemName=widgitlabs.widgit-monokai)

This theme is a tweaked version of [One Monokai](https://marketplace.visualstudio.com/items?itemName=azemoh.one-monokai)
by Joshua Azemoh using our branding colors. It's part of a project to provide a
standardized, consistent experience across the variety of platforms and software
that we officially use. Anyone is welcome to use it, fork it, or improve it at
will. Enjoy!

## Installation

Press `ctl/command + shift + p` to launch the command palette, then run

```plaintext
ext install widgit-monokai
```

## Screenshot

![Theme Screenshot](https://gitlab.com/widgitlabs/widgit-monokai/vscode/-/raw/master/preview.jpg)

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/widgit-monokai/vscode/issues?state=open)!

## Changelog

You can take a look at the change log [here](https://gitlab.com/widgitlabs/widgit-monokai/vscode/-/blob/master/CHANGELOG.md)
